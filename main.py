import api.create_cards as cr
import api.read_list as rl
import database.word_table as wt
import database.word as w


app_id = 'a25b2974'
app_key = 'fa3026e50dddff867874b8293f84b703'
endpoint = "entries"
language_code = "en-us"

create = cr.CreateCards(app_id, app_key, endpoint, language_code)
words = rl.ReadList("words.txt").list_of_words()
create.multiple_cards(words)

# word = w.Word("break", "I will break your bones", "Rick and Morty", 100, 110)
# base = wt.WordTable()
# base.insert(word)
# print(base.select("break"))

