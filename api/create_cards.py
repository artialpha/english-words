import api.api as api
import api.lexical_entry as entry
import api.group_entries as group
import io
import re
from pyinflect import getAllInflections


class CreateCards:
    def __init__(self, app_id, app_key, endpoint, language_code):
        self.api = api.Api(app_id, app_key, endpoint, language_code)

        self.group = group.GroupEntries("words.txt")
        self.group.create_group()

    def multiple_cards(self, words):
        for x in words:
            print(x)
            self.one_card(x)

    def one_card(self, word):
        result = self.api.word_json(word[0])
        for x in result["results"]:
            ent = x["lexicalEntries"][0]["entries"][0]["senses"]

        entries = entry.LexicalEntry(result["results"])
        with io.open("cards.txt", "a", encoding="utf-8") as file_object:
            for sense in entries.senses:
                file_object.write(self.delete_inflected_word(word[0], sense.example))
                file_object.write(self.question_line(word))
                file_object.write(CreateCards.embolden(word[0], sense.example) + "|")
                file_object.write(word[1]+"|")
                file_object.write(sense.definition + "|")
                file_object.write(sense.ipa + "|")
                print(entries.entry_content())
                file_object.write(CreateCards.embolden(word[0], entries.entry_content()) + "\n")

    @staticmethod
    def delete_inflected_word(word, to_write):
        inf = CreateCards.inflected_word_list(word)
        to_write = ' '.join(i if i not in inf else "..." for i in re.findall(r"[\w']+|[.,!?;]", to_write))
        to_write += '|'
        return to_write

    @staticmethod
    def inflected_word_list(word):
        inflected = [x[0] for x in list(getAllInflections(word).values())]
        inflected.extend([x.capitalize() for x in inflected])
        return list(set(inflected))

    @staticmethod
    def question_line(word):
        if word[1] == 'X':
            return "...|"
        else:
            return word[1]+"|"

    @staticmethod
    def embolden(word, text):
        inflected = CreateCards.inflected_word_list(word)
        positions = []
        for inf in inflected:
            positions.extend([(a.start(), a.end()) for a in list(re.finditer(inf, text))])
        positions.sort(reverse=True)
        for pos in positions:
            text = text[:pos[0]] + "<b>" + text[pos[0]:pos[1]] + "</b>" + text[pos[1]:]
        return text


