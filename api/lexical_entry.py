import api.sense as sense
import itertools


class LexicalEntry:
    def __init__(self, results):
        #x = [[sense.Sense(x["definitions"][0], example['text']) for example in x["examples"]] for x in senses]

        y = []
        for r in results:
            for x in r["lexicalEntries"][0]["entries"][0]["senses"]:
                if 'examples' in x:
                    y.append([sense.Sense(x["definitions"][0], example['text']) for example in x["examples"]])
                    if "subsenses" in x:
                        for sub in x["subsenses"]:
                            if 'examples' in sub:
                                y.append(sense.Sense(sub["definitions"][0], example['text']) for example in sub["examples"])
        self.senses = list(itertools.chain.from_iterable(y))

    def entry_content(self):
        content = ""
        for count, value in enumerate(self.senses, 1):
            content += '<div>' + str(count) + '<div>' + value.sense_content() + '<div>'
            content += '<hr>'
        return content
