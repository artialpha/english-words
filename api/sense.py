import eng_to_ipa as ipa


class Sense:
    def __init__(self, definition, example):
        self.definition = definition + '<div>' + ipa.convert(definition) + '<div>'
        self.example = example
        self.ipa = ipa.convert(example)

    def sense_content(self):
        return '<div>' + self.definition + '<div>' + self.example + '<div>' + self.ipa

    @staticmethod
    def embolden(word, text):
        l_word = len(word)
        position_word_start = text.find(word)
        position_word_end = position_word_start + l_word
        text = text[:position_word_start] + "<b>" + word + "</b>" + text[position_word_end:]
        return text
