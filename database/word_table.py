import sqlite3
import database.word as word


class WordTable:
    def __init__(self):
        self.conn = sqlite3.connect('words.db')
        cur = self.conn.cursor()
        cur.execute("""CREATE TABLE IF NOT EXISTS words(
                    word text,
                    sentence text,
                    source text,
                    start integer,
                    end integer,
                    UNIQUE(sentence)
                    )""")

    def insert(self, word_to_add):
        cur = self.conn.cursor()
        cur.execute("INSERT OR IGNORE INTO words VALUES (:word, :sentence, :source, :start, :end)",
                  {'word': word_to_add.word, 'sentence': word_to_add.sentence, 'source': word_to_add.source,
                   'start': word_to_add.start, 'end': word_to_add.end})
        self.conn.commit()

    def select(self, word_to_take):
        cur = self.conn.cursor()
        cur.execute("SELECT * FROM words WHERE word=:word", {'word': word_to_take})
        value = cur.fetchall()
        self.conn.commit()
        return value



