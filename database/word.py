

class Word:

    def __init__(self, word, sentence, source, start, end):
        # source can be a book, a movie or even a song
        # start and end in a movie/song - seconds where a fragment with the sentence starts and ends
        # for book can be a position of the word in a book
        self.word = word
        self.sentence = sentence
        self.source = source
        self.start = start
        self.end = end
